import React, {Component} from 'react';
import './Btn.scss'

class Btn extends Component {

    render() {
        const {backgroundColor, text, onClick} = this.props;

        console.log('btn props', this.props)

            // const classes = classNames(
            //     'btn',
            // )

        return (
            <button
                className='btnMod'
                onClick={onClick}
                style = {{backgroundColor:backgroundColor}}
            >{text}</button>
        )
    }
}

export default Btn