import React, {Component} from 'react';
import Btn from "../Btn/Btn";
import './Modal.scss';
import CloseIcon from '@material-ui/icons/Close';

class Modal extends Component {
    render() {
        const {displayModalId, modals, buttons, setActive} = this.props

        const modalInfo = displayModalId ? this.chooseModal(displayModalId, modals) : '';

        console.log('modal prop!!!!!', buttons)

        // const btnSubmit = <Btn/>

        const modal =
            <div className="modalOverlay" onClick={setActive}>
                <div className="modalWindow" onClick={event => event.stopPropagation()}>
                    <div className="modalHeader">
                        <p className='modalHeader__text'>{modalInfo.header}</p>
                        <div onClick={setActive}>
                            <CloseIcon name={'close'} fontSize='large' color={'red'} />
                        </div>
                    </div>
                    <div className="modalBody">
                        <p>{modalInfo.text}</p>
                    </div>
                    <div className="modalFooter">
                        <Btn {...buttons.btnSubmit}/>
                        <Btn {...buttons.btnCancel}/>
                    </div>
                </div>
            </div>
        const ifModal = displayModalId ? modal : '';
        return (
            ifModal
        )
    }

    chooseModal(displayModalId, modals) {
        if (displayModalId === 'first') {
            return modals.firstMod;
        } else if (displayModalId === 'second') {
            return modals.secondMod;
        }
    }
}

export default Modal