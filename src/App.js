import * as React from "react"
import logo from './logo.svg';
import './App.css';
import Btn from "./Component/Btn/Btn";
import Chosemodal from "./Component/Chosemodal/Chosemodal";
import Modal from "./Component/Modal/Modal";

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            displayModalId: '',

            buttons: {
                btnSubmit: {
                    backgroundColor: 'green',
                    text: 'Submit',
                    onClick: () => {
                        console.log('submit')
                    }
                },
                btnCancel: {
                    backgroundColor: '#CCCC00',
                    text: 'Cancel',
                    onClick: this.onCancel
                }
            },

            modals: {
                firstMod: {
                    header: 'First modal',
                    closeButton: true,
                    text: 'same text for first',
                },
                secondMod: {
                    header: 'Second modal',
                    closeButton: true,
                    text: 'same text for second',
                }
            }

        };
    }

    onCancel = () => {
        console.log('cancel')
    }

    setActive = () => {
        this.setState({displayModalId: ''})
    }

    showModal = (mod) => {
        this.setState({displayModalId: mod})
        console.log('mod', mod)
    }

    render() {
        const {displayModalId, modals, buttons} = this.state;

        return (
            <div className="App">
                <Modal setActive={this.setActive} modals={modals} displayModalId={displayModalId} buttons={buttons}/>
                {/*<Btn {...buttons.btnCancel}/>*/}
                <button onClick={() => this.showModal('first')} className="btnModal">Show first modal</button>
                <button onClick={() => this.showModal('second')} className="btnModal">Show second modal</button>
            </div>
        );
    }

}

export default App;
